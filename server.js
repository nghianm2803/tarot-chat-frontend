const express = require('express')
const app = express();
const port = 3000;
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.raw());
require('dotenv').config();

app.post('/google-login', (req, res) => {
    console.log('Got body:', req.body);
    let urlParam = new URLSearchParams(req.body).toString();
    let newUrl = process.env.VUE_APP_URL_MAIN + '/login?' + urlParam;
    console.log(newUrl)
    res.redirect(301, newUrl)
})

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})