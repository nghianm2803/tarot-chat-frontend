import { axiosGetInstanceBackend, axiosPostInstanceBackend, axiosPutInstanceBackend } from "../helpers/axios-config";

interface dataCreateOrderDto extends URLSearchParams {
  order_service_id: number,
  order_quantity: number,
  order_note?: string
}
interface dataCreateMetaOrderDto extends URLSearchParams {
  order_id: number,
  meta_key: string,
  meta_value: string
}
class OrderClass {
  getOrders = async (page: number, limit: number, orderStatus: string, authCode: string) => {
    const config = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "X-Authorization": authCode,
      },
    };
    let dataGet = `/orders?page=${page}&limit=${limit}`;
    if (orderStatus) {
      dataGet = `/orders?page=${page}&limit=${limit}&order_status=${orderStatus}`;
    }
    return await axiosGetInstanceBackend(process.env.mainBackendUrl + dataGet, config);
  }
  getDetailOrder = async (orderId: number, authCode: string) => {
    const config = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "X-Authorization": authCode,
      },
    };
    let dataGet = `/orders/${orderId}`;
    return await axiosGetInstanceBackend(process.env.mainBackendUrl + dataGet, config);
  }

  // Get transaction link to admin transaction
  getDetailTransaction = async (transactionId: number, authCode: string) => {
    const config = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "X-Authorization": authCode,
      },
    };
    let dataGet = `/transaction/${transactionId}`;
    return await axiosGetInstanceBackend(process.env.mainBackendUrl + dataGet, config);
  }

  getOrderMeta = async (orderId: number, authCode: string) => {
    const config = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "X-Authorization": authCode,
      },
    };
    let dataGet = `/orders_meta/${orderId}`;
    return await axiosGetInstanceBackend(process.env.mainBackendUrl + dataGet, config);
  }
  createOrderMeta = async (dataCreate: dataCreateMetaOrderDto, authCode: string) => {
    const config = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "X-Authorization": authCode,
      },
    };
    const params = new URLSearchParams(dataCreate);
    return await axiosPostInstanceBackend(process.env.mainBackendUrl + `/orders_meta`, params, config);
  }
  createNewOrder = async (dataCreate: dataCreateOrderDto, authCode: string) => {
    const config = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "X-Authorization": authCode,
      },
    };
    const params = new URLSearchParams(dataCreate);
    return await axiosPostInstanceBackend(process.env.mainBackendUrl + `/orders`, params, config);
  }
  updateOrder = async (orderId: number, statusOrder: string, authCode: string) => {
    const config = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "X-Authorization": authCode,
      },
    };
    let dataUrl = `/orders/${orderId}/${statusOrder}`;
    return await axiosPutInstanceBackend(process.env.mainBackendUrl + dataUrl, {}, config);
  }
}

export const orderService =  new OrderClass();
