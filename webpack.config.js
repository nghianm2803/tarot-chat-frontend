var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    mode: 'development',
    resolve: {
        extensions: ['.ts', '.js', '.vue', '.json'],
        alias: {
            'vue$': 'vue/dist/vue.esm.js'
        }
    },

    module: {
        loaders: [{
            test: /\.vue$/,
            loader: 'vue'
        },
        {
            test: /\.s[a|c]ss$/,
            loader: 'style!css!sass'
        }
        ],
        rules: [{
            test: /\.vue?$/,
            exclude: /(node_modules)/,
            use: 'vue-loader'
        },
        {
            test: /\.js?$/,
            exclude: /(node_modules)/,
            use: 'babel-loader'
        }
        ]
    },
    plugins: [new HtmlWebpackPlugin({
        template: './src/index.html'
    })],
    devServer: {
        historyApiFallback: true
    },
    externals: {
    },
    vue: {
        loaders: {
            scss: 'style!css!sass'
        }
    }
}